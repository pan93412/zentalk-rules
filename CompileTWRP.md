# 如何編譯 TWRP Touch Recovery？
翻譯進度：10% | 作者：pan93412

## 概述
所有 TWRP 3.x 原始碼皆為開源，因此您可以自行編譯。本教學不打算以 Step-by-step（步驟式引導）及 Word-for-word（逐字引導）的類型教學。因此若您不熟悉基本 Linux 指令或 AOSP 編譯，那您或許該停止考慮編譯 TWRP。

目前您可用 Omni 5.1、Omni 6.0、Omni 7.1、Omni 8.1、CM 12.1、CM 13.0、CM 14.1 或 CM 15.1 的原始碼，而我比較推薦 Omni 7.1。您可以於 CM 編譯，但可能會遇到些許問題。若您並不知道如何解決一些小問題，那您應使用 Omni。

假如您正在使用 CM，您將需要在 CM/bootable/recovery-twrp 資料夾中放入 TWRP，並在您的 BoardConfig.mk 檔案中設定 `RECOVERY_VARIANT := twrp`。TWRP 的原始碼可以在 [這裡](https://github.com/omnirom/android_bootable_recovery) 找到。<br />
選擇可以使用的 **最新分支** ，這步驟對 Omni 來說是多餘的，因為 Omni 已經預先包含了 TWRP 來源，但是，當您使用較舊版本的 Omni時，您可能會想要從最新分支抓取最新的 TWRP 來源。（最新的分支能在舊編譯樹上順利編譯）

若您只是單純有興趣想嘗試編譯 TWRP，您可能會想在較小的編譯樹上嘗試。您可以嘗試使用這個 Manifest，它應該可以在大部分的情況下運作，但在某些情況下，您的編譯樹可能需要比這個 Manifest 提供的來源庫還要更多：
<https://github.com/minimal-manifest-twrp>

## 編譯之前
注意：若您增加或修改任何標記 (flag)，您將需要在編譯之前執行 `make clean` 或 `make clobber`，否則您的標記變更將不會生效。

現在您已經有了原始碼。您將需要為您的裝置設定或修改一小部分的標記。找到您裝置的 BoardConfig.mk，其可能在 devices/ *製造商* / *手機 codename* 資料夾內（例如：devices/lge/hammerhead/BoardConfig.mk）。

您的 board 設定需要包含系統架構與平臺設定。若您使用他人建立的裝置設定檔案，這些設定通常已經包含在內，但是，假如您建立自己的裝置設定檔案，您將需要增加這些設定至您的設定檔案內。若您選擇不加入，Recovery 可能會在開機時故障，而您將看到 Teamwin 啟動畫面在螢幕一直閃爍。 

We usually put all of our flags at the bottom of the BoardConfig.mk under a heading of #twrp For all devices you'll need to tell TWRP what theme to use. This TW_THEME flag replaces the older DEVICE_RESOLUTION flag. TWRP now uses scaling to stretch any theme to fit the screen resolution. There are currently 5 settings which are: portrait_hdpi, portrait_mdpi, landscape_hdpi, landscape_mdpi, and watch_mdpi. For portrait, you should probably select the hdpi theme for resolutions of 720x1280 and higher. For landscape devices, use the hdpi theme for 1280x720 or higher.
TW_THEME := portrait_hdpi
